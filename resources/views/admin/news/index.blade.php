@extends('layouts.admin')
@section('title')
News
@endsection
@section('content')
<a href="javascript:void(0)" class="btn btn-primary btn-icon-split shadow" data-toggle="modal" data-target="#create">
  <span class="icon text-white-50">
    <i class="fas fa-plus"></i>
  </span>
  <span class="text">Add Data</span>
</a>
@include('admin.news.create')
<div class="card shadow mt-4 mb-4">
   <!-- Card Body -->
   <div class="card-body">
      <div class="table-responsive">
         <table class="table table-bordered table-stripped">
            <thead>
               <tr>
                  <td>
                     No
                  </td>
                  <td>
                     Title
                  </td>
                  <td>
                     Status
                  </td>
                  <td>
                     Created at
                  </td>
                  <td>
                     Action
                  </td>
               </tr>
            </thead>
            <tbody>
               @if($models->count() == 0)
               <tr>
                  <td colspan="6" align="center">
                     No data
                  </td>
               </tr>
               @endif
               @foreach($models as $key => $item)
               <tr>
                  <td>
                     {{ ($models->perPage() * ($models->currentPage() - 1)) + ($key + 1) }}
                  </td>
                  <td>
                     {{$item->title}}
                  </td>
                  <td>
                    @if($item->status == 1)
                     <span class="badge badge-success">Published</span>
                    @else
                     <span class="badge badge-secondary">Draft</span>
                    @endif
                  </td>
                  <td>
                     {{Carbon\Carbon::parse($item->created_at)->format('d-m-Y H:i:s')}}
                  </td>
                  <td>
                     <form action="{{route('admin.news.destroy',$item->id)}}" method="POST" onsubmit="return confirm('Are you sure?')">
                        @method('DELETE')
                        @csrf
                        <a href="javascript:void(0)" class="btn btn-warning btn-circle btn-sm" data-toggle="modal" data-target="#edit-{{$item->id}}">
                        <i class="fas fa-pen"></i>
                        </a>
                        <button class="btn btn-danger btn-circle btn-sm">
                        <i class="fas fa-trash"></i>
                        </button>
                     </form>
                     <div class="modal fade" id="edit-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                       <div class="modal-dialog modal-lg" role="document">
                         <div class="modal-content">
                           <div class="modal-header">
                             <h5 class="modal-title" id="exampleModalLabel">Add Data</h5>
                             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                               <span aria-hidden="true">&times;</span>
                             </button>
                           </div>
                           <form action="{{route('admin.news.update', $item->id)}}" method="POST" enctype="multipart/form-data">
                             @csrf
                             @method('PUT')
                             <div class="modal-body">
                               <div class="form-group">
                                 <label for="" class="label">Title</label>
                                 <input type="text" value="{{$item->title}}" class="form-control" autocomplete="off" name="title">
                               </div>
                               <div class="form-group">
                                 <label for="" class="label">Featured Image</label>
                                 <div class="input-group">
                                   <div class="custom-file">
                                     <input type="file" name="img" class="custom-file-input">
                                     <label class="custom-file-label" for="logo">Choose file</label>
                                   </div>
                                 </div>
                                 @if($item->img != "")
                                   <img style="width:30%; margin-top:10px;" src="{{$item->getImage()}}" alt="{{$item->title}}">
                                 @endif
                               </div>
                               <div class="form-group">
                                 <label for="" class="label">Content</label>
                                 <textarea class="form-control my-editor" name="content" rows="8" cols="80" autocomplete="off">{!! $item->content !!}</textarea>
                                 <input type="hidden" name="status" value="1">
                               </div>
                               <div class="form-group">
                                  <label for="" class="label">Status</label>
                                  <select name="status" class="form-control">
                                  <option value="1"  @if($item->status == '1') selected @endif>Active</option>
                                  <option value="0"  @if($item->status == '0') selected @endif>Draft</option>
                                  </select>
                               </div>
                             </div>
                             <div class="modal-footer">
                               <button type="reset" class="btn btn-secondary" data-dismiss="modal">Close</button>
                               <button type="submit" class="btn btn-primary">Save changes</button>
                             </div>
                           </form>
                         </div>
                       </div>
                     </div>
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
      {{ $models->appends(\Request::query())->links() }}
   </div>
</div>
@endsection
@section('script')
@endsection
