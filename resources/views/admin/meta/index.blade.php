@extends('layouts.admin')
@section('title')
Setting
@endsection
@section('content')
<div class="card shadow mb-4">
  <div class="card-body">
    <form class="row" action="{{route('admin.setting.update',$models->id)}}" method="POST" enctype="multipart/form-data">
      @csrf
      @method('PUT')
      <div class="col-lg-6">
        <div class="form-group">
          <label class="text-muted" for="site_title">Site Title</label>
          <input type="text" class="form-control" id="site_title" name="title" required placeholder="Site Title" value="{{$models->title}}">
        </div>
        <div class="form-group">
          <label class="text-muted" for="keyword">Site Keyword</label>
          <input type="text" class="form-control" id="keyword" name="keyword" required placeholder="Bali, Travel, Denpasar, Bali Travel News, News" value="{{$models->keyword}}">
        </div>
        <div class="form-group">
          <label class="text-muted" for="description">Description</label>
          <textarea class="form-control" id="description" name="description" required placeholder="Site Description" rows="8" cols="80">{{$models->description}}</textarea>
        </div>
      </div>
      <div class="col-lg-6">
        <div class="form-group">
          <label class="text-muted" for="logo">Logo</label>
          <div class="input-group">
            <div class="custom-file">
              <input type="file" name="logo" class="custom-file-input" id="logo">
              <label class="custom-file-label" for="logo">Choose file</label>
            </div>
          </div>
          @if($models->logo != "")
            <img style="width: 150px;border-radius: 5px;margin-top:10px;background: #ddd;padding: 10px;" src="{{$models->getImage()}}" alt="{{$models->site_title}}">
          @endif
        </div>
      </div>
      <div class="col-md-12 mt-3">
        <div class="form-group">
           <button type="submit" class="btn btn-success">Save Change</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection
@section('script')

@endsection
