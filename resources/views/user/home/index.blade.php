@extends('layouts.user')
@section('content')
<div class="dashboard-area">
  <div class="container-fluid">
    <div class="row align-items-center mb-30">
      <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
          <h4 class="mb-2 font-20">COVID-19 Coronavirus Data</h4>
        </div>
        <div class="dashboard-infor-mation">
          <div class="dashboard-clock d-flex flex-wrap align-items-center">
            <div id="dashboardDate"></div>
            <span class="ml-1"> : </span>
            <ul class="d-flex align-items-center justify-content-end ml-2">
              <li id="hours"></li>
              <li>:</li>
              <li id="min"></li>
              <li>:</li>
              <li id="sec"></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row" id="statistics">
      <div class="col-lg-7 mb-30">
        <div class="card h-100">
          <div class="card-body">
            <h5 class="card-title">Data 1 Bulan Terakhir</h5>
            <div id="apex-area-chart"></div>
            <div class="loading-chart" v-if="loading">
              <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="39px" height="39px" viewBox="0 0 128 128" xml:space="preserve"><g><path d="M75.4 126.63a11.43 11.43 0 0 1-2.1-22.65 40.9 40.9 0 0 0 30.5-30.6 11.4 11.4 0 1 1 22.27 4.87h.02a63.77 63.77 0 0 1-47.8 48.05v-.02a11.38 11.38 0 0 1-2.93.37z" fill="#2231f6" fill-opacity="1"/><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1600ms" repeatCount="indefinite"></animateTransform></g></svg>
            </div>
          </div>
        </div>
      </div>
      <!-- Category -->
      <div class="col-lg-5">
        <div class="row worldwide-stats">
          <div class="col-md-6">
            <div class="card mb-30 bg-primary">
              <div class="card-body">
                <div class="tracker-block__icon">
                  <img src="/data/img/bg-img/1.png" alt="icon" />
                </div>
                <div class="corona-count-content">
                  <h4 class="text-white">Total Kasus</h4>
                  <div class="loading" v-if="loading">
                    <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="39px" height="39px" viewBox="0 0 128 128" xml:space="preserve"><g><path d="M75.4 126.63a11.43 11.43 0 0 1-2.1-22.65 40.9 40.9 0 0 0 30.5-30.6 11.4 11.4 0 1 1 22.27 4.87h.02a63.77 63.77 0 0 1-47.8 48.05v-.02a11.38 11.38 0 0 1-2.93.37z" fill="#ffffff" fill-opacity="1"/><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1600ms" repeatCount="indefinite"></animateTransform></g></svg>
                  </div>
                  <h2 class="text-white" v-if="!loading">
                    <span class="cases-no infected">@{{dashboard.total.jumlah_positif}}</span>
                  </h2>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card mb-30 bg-success">
              <div class="card-body">
                <div class="tracker-block__icon">
                  <img src="/data/img/bg-img/4.png" alt="icon" />
                </div>
                <div class="corona-count-content">
                  <h4 class="text-white">Total Sembuh</h4>
                  <div class="loading" v-if="loading">
                    <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="39px" height="39px" viewBox="0 0 128 128" xml:space="preserve"><g><path d="M75.4 126.63a11.43 11.43 0 0 1-2.1-22.65 40.9 40.9 0 0 0 30.5-30.6 11.4 11.4 0 1 1 22.27 4.87h.02a63.77 63.77 0 0 1-47.8 48.05v-.02a11.38 11.38 0 0 1-2.93.37z" fill="#ffffff" fill-opacity="1"/><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1600ms" repeatCount="indefinite"></animateTransform></g></svg>
                  </div>
                  <h2 class="text-white" v-if="!loading">
                    <span class="cases-no recovered">@{{dashboard.total.jumlah_sembuh}}</span>
                  </h2>
                  </h2>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card mb-30 bg-danger">
              <div class="card-body">
                <div class="tracker-block__icon">
                  <img src="/data/img/bg-img/2.png" alt="icon" />
                </div>
                <div class="corona-count-content">
                  <h4 class="text-white">Total Meninggal</h4>
                  <div class="loading" v-if="loading">
                    <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="39px" height="39px" viewBox="0 0 128 128" xml:space="preserve"><g><path d="M75.4 126.63a11.43 11.43 0 0 1-2.1-22.65 40.9 40.9 0 0 0 30.5-30.6 11.4 11.4 0 1 1 22.27 4.87h.02a63.77 63.77 0 0 1-47.8 48.05v-.02a11.38 11.38 0 0 1-2.93.37z" fill="#ffffff" fill-opacity="1"/><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1600ms" repeatCount="indefinite"></animateTransform></g></svg>
                  </div>
                  <h2 class="text-white" v-if="!loading">
                    <span class="cases-no deaths">@{{dashboard.total.jumlah_meninggal}}</span>
                  </h2>
                  </h2>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="card mb-30 bg-warning">
              <div class="card-body">
                <div class="tracker-block__icon">
                  <img src="/data/img/bg-img/3.png" alt="icon" />
                </div>
                <div class="corona-count-content">
                  <h4 class="text-white">Kasus Aktif</h4>
                  <div class="loading" v-if="loading">
                    <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="39px" height="39px" viewBox="0 0 128 128" xml:space="preserve"><g><path d="M75.4 126.63a11.43 11.43 0 0 1-2.1-22.65 40.9 40.9 0 0 0 30.5-30.6 11.4 11.4 0 1 1 22.27 4.87h.02a63.77 63.77 0 0 1-47.8 48.05v-.02a11.38 11.38 0 0 1-2.93.37z" fill="#ffffff" fill-opacity="1"/><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1600ms" repeatCount="indefinite"></animateTransform></g></svg>
                  </div>
                  <h2 class="text-white" v-if="!loading">
                    <span class="cases-no current_cases">@{{dashboard.total.jumlah_dirawat}}</span>
                  </h2>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row" id="location">
      <div class="col-12">
        <div class="card box-margin">
          <div class="card-body">
            <h4 class="card-title">Daerah Terdampak</h4>
            <div class="list-view prov-responsive table-responsive position-relative" id="list-view" style="min-height: 300px;">
              <div class="loading-chart mt-3" v-if="loading">
                <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="39px" height="39px" viewBox="0 0 128 128" xml:space="preserve"><g><path d="M75.4 126.63a11.43 11.43 0 0 1-2.1-22.65 40.9 40.9 0 0 0 30.5-30.6 11.4 11.4 0 1 1 22.27 4.87h.02a63.77 63.77 0 0 1-47.8 48.05v-.02a11.38 11.38 0 0 1-2.93.37z" fill="#2231f6" fill-opacity="1"/><animateTransform attributeName="transform" type="rotate" from="0 64 64" to="360 64 64" dur="1600ms" repeatCount="indefinite"></animateTransform></g></svg>
              </div>
              <table class="list-view__table display table">
                <thead>
                  <tr>
                    <th>Kota</th>
                    <th>Total Kasus</th>
                    <th>Kasus Baru</th>
                    <th>Total Meninggal</th>
                    <th>Meninggal %</th>
                    <th>Baru Meninggal</th>
                    <th>Total Sembuh</th>
                    <th>Sembuh %</th>
                    <th>Kasus Aktif</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="item in prov.list_data">
                    <td class="sorting_1">@{{ item.key }}</td>
                    <td>@{{ item.jumlah_kasus }}</td>
                    <td>+ @{{ item.penambahan.positif }}</td>
                    <td>@{{ item.jumlah_meninggal }}</td>
                    <td>@{{ formatDecimal((item.jumlah_meninggal * 100) / item.jumlah_kasus) }}%</td>
                    <td>+ @{{ item.penambahan.meninggal }}</td>
                    <td>@{{ item.jumlah_sembuh }}</td>
                    <td>@{{ formatDecimal((item.jumlah_sembuh * 100) / item.jumlah_kasus) }}%</td>
                    <td>@{{ item.jumlah_dirawat }}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row" id="news">
      <div class="col-12">
        <div class="card mb-30">
          <div class="card-body">
            <div class="row">
              <div class="col-12">
                <h4 class="card-title">Berita Terbaru</h4>
              </div>
              <div class="col-md-6 col-lg-3 mb-3" v-for="(item, index) in post" v-if="index < postShow" data-aos="slide-up">
                <div class="single-blog--area mb-20">
                  <div class="thumb">
                    <img style="width: 100%; height: 210px; object-fit: cover; object-position: center;" :src="item.img" :alt="item.title">
                  </div>
                  <div class="text-content">
                    <h5><a :href="'/news/' + item.slug">@{{item.title}}</a></h5>
                    <p class="mb-0">@{{item.content}}...</p>
                  </div>
                </div>
              </div>
              <div class="col-12">
                <button v-if="!allLoaded" class="btn btn-primary" @click="showMore()">Show More Post</button>
                <button v-if="allLoaded" class="btn btn-primary" disabled>All Post Loaded</button>
                <svg class="mx-2" v-if="loadMore" width="35" height="35" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg" stroke="#2231f6">
                  <g fill="none" fill-rule="evenodd">
                    <g transform="translate(1 1)" stroke-width="2">
                      <circle stroke-opacity=".5" cx="18" cy="18" r="18"/>
                      <path d="M36 18c0-9.94-8.06-18-18-18">
                        <animateTransform
                          attributeName="transform"
                          type="rotate"
                          from="0 18 18"
                          to="360 18 18"
                          dur="1s"
                          repeatCount="indefinite"
                        />
                      </path>
                    </g>
                  </g>
                </svg>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<script>
  var app = new Vue({
   el: '#app',
   data () {
     return {
       dashboard: [],
       prov: [],
       chart: [],
       post: [],
       postShow: 4,
       loadMore: false,
       allLoaded: false,
       // LOADING
       loading: false,
       loadingTable: false,
       chartOptions: {
           chart: {
               height: 305,
               type: 'area',
               toolbar: {
                   show: false
               },
               zoom: {
                   type: 'x',
                   enabled: false,
                   autoScaleYaxis: true
               },
           },
           dataLabels: {
               enabled: false
           },
           stroke: {
               curve: 'smooth',
           },
           colors: ['#727cf5', '#04d39f', '#fa5c7c'],
           series: [
             {
                 name: 'Case',
                 data: [],
             }, {
                 name: 'Recovered',
                 data: [],
             }, {
                 name: 'Death',
                 data: [],
             }
           ],
           legend: {
               show: false,
           },
           xaxis: {
               type: 'datetime',
               categories: [],
               axisBorder: {
                   show: true,
                   color: 'rgba(0,0,0,0.05)'
               },
               axisTicks: {
                   show: true,
                   color: 'rgba(0,0,0,0.05)'
               }
           },
           grid: {
               row: {
                   colors: ['transparent', 'transparent'],
                   opacity: .2
               },
               borderColor: 'rgba(0,0,0,0.05)'
           },
           tooltip: {
               x: {
                   format: 'dd/MM/yy HH:mm'
               },
           }
       },
     }
   },
   methods: {
     formatDecimal(value) {
      let val = (value).toFixed(2)
      return val
     },
     showMore() {
       this.loadMore = true;
       if (this.postShow == this.post.length) {
           this.allLoaded = true;
           this.loadMore = false;
       } else {
         setTimeout(()=>{
             this.postShow += 4
             this.loadMore = false;
         },2000);
       }
     },
     Chart(){
       var chart = new ApexCharts(document.querySelector("#apex-area-chart"), this.chartOptions);
       document.getElementById('apex-area-chart').innerHTML = '';
       chart.render();
     },
     loadDashboard(){
       this.loading = true
       axios.get("https://data.covid19.go.id/public/api/update.json")
       .then((response) => {
         if(response.data.length == 0){
           this.dashboard = null;
         } else {
           this.dashboard = response.data.update
           this.chart = response.data.update.harian.slice(Math.max(response.data.update.harian.length - 30, 0))

           this.chartOptions.series[0].data = this.chart.map(function (item) {
            return item.jumlah_positif.value
           });

           this.chartOptions.series[1].data = this.chart.map(function (item) {
            return item.jumlah_meninggal.value
           });

           this.chartOptions.series[2].data = this.chart.map(function (item) {
            return item.jumlah_sembuh.value
           });

           this.chartOptions.xaxis.categories = this.chart.map(function (item) {
            return item.key_as_string
           });
         }

         this.loading = false
         // console.log(this.dashboard)
         // console.log(this.chart)
       }).catch((error) => {
         console.log(error);
       })
     },
     loadTable(){
       this.loadingTable = true
       axios.get("https://data.covid19.go.id/public/api/prov.json")
       .then((response) => {
         if(response.data.length == 0){
           this.prov = null;
         } else {
           this.prov = response.data
         }
         this.loadingTable = false
         // console.log(this.prov)
       }).catch((error) => {
         console.log(error);
       })
     },
     loadNews(){
       this.loadingTable = true
       axios.get("{{route('api.v1.news')}}")
       .then((response) => {
         if(response.data.length == 0){
           this.post = null;
         } else {
           this.post = response.data
         }
         this.loadingTable = false
         // console.log(this.post)
       }).catch((error) => {
         console.log(error);
       })
     },
   },
   created(){
     this.loadDashboard()
     this.loadTable()
     this.loadNews()
   },
   updated: function () {
     this.Chart()
   },
  })
  AOS.init();
</script>
@endsection
