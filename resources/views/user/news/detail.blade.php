@extends('layouts.user')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-8 posts-list blog-details-area mb-30">
      <div class="card">
        <div class="card-body">
          <div class="single-post">
            <div class="feature-img">
              <img class="img-fluid w-100" src="{{$models->getImage()}}" alt="{{$models->title}}">
            </div>
            <div class="blog_details">
              <h1 class="my-4">{{$models->title}}</h1>
              {!! $models->content !!}
            </div>
          </div>
          <div class="share-icon">
            <h4 class="card-title mb-20">Share this story</h4>
            <!-- ShareThis BEGIN --><div class="sharethis-inline-share-buttons"></div><!-- ShareThis END -->
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="card mb-30">
        <div class="card-body pb-0">
          <div class="blog_right_sidebar mt-sm-100">
            <aside class="single_sidebar_widget post_category_widget">
              <h4 class="widget_title">Recent Post</h4>
              @foreach($recents as $item)
              <div class="mb-20">
                <h6 class="font-15"><a href="{{route('news-detail', $item->slug)}}">{{$item->title}}</a></h6>
                <p class="mb-10">{{$item->content()}}</p>
                <span class="text-primary"><i class="fa fa-clock-o" aria-hidden="true"></i> {{$item->date()}}</span>
              </div>
              @endforeach
            </aside>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
@endsection
