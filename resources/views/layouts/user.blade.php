@php($models = \App\Meta::find(1))
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="keywords" content="{{$models->keyword}}">
    <meta name="description" content="{{$models->description}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Required meta tags -->
    <title>{{$models->title}} - Statistik Covid</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios@0.20.0/dist/axios.min.js"></script>
    <!-- Favicon -->
    <link rel="icon" href="/icon.png">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <!-- Master Stylesheet CSS -->
    <link rel="stylesheet" href="/data/style.css">
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=5f93cebb90c74500124aaaaf&product=sop' async='async'></script>
  </head>
  <body>
    <!-- Preloader -->
    <div class="loading-area">
      <div class="loading-box"></div>
      <div class="loading-pic">
        <div class="loader-animation-outer">
          <div class="rotate-center">
            <div class="scale-up-center">
              <img src="/data/img/core-img/corona.png" alt="">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Preloader -->
    <!-- ======================================
      ******* Main Page Wrapper Area **********
      ======================================= -->
    <div class="page-wrapper" id="app">
      <div class="horizontal-menu">
        <nav class="navbar top-navbar col-lg-12 col-12 p-0">
          <div class="container-fluid">
            <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
              <a class="navbar-brand brand-logo" href="/"><img src="{{$models->getImage()}}" alt="{{$models->title}}"></a>
              <a class="navbar-brand brand-logo-mini" href="/"><img src="/small-logo.png" alt="{{$models->title}}"></a>
            </div>
            <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
              <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
                <span class="ti-menu"></span>
              </button>
            </div>
            <div class="preheader-area d-none d-lg-block">
              <div class="preheader-content-wrap d-flex align-items-center">
                <h6 class="mb-0 mr-4 text-white">Butuh Bantuan Lebih?</h6>
                <a href="mailto:gempong123@gmail.com" class="btn btn-default">Hubungi Kami</a>
              </div>
            </div>
          </div>
        </nav>
        <nav class="bottom-navbar">
          <div class="container-fluid">
            <ul class="nav page-navigation">
              <li class="nav-item">
                <a class="nav-link" href="/">
                <i class="bx bx-home-heart menu-icon"></i>
                <span class="menu-title">Beranda</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#statistics">
                <i class="bx bx-bar-chart-square menu-icon"></i>
                <span class="menu-title">Statistik</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#location">
                <i class="bx bx-map menu-icon"></i>
                <span class="menu-title">Daerah Terdampak</span>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#news">
                <i class="bx bx-news menu-icon"></i>
                <span class="menu-title">Update Berita</span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </div>
      <!-- Main Page Area -->
      <div class="main-page-content">
        @yield('content')
      </div>
      <!-- Footer Area -->
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Footer Area -->
            <footer class="footer-area d-sm-flex justify-content-center align-items-center justify-content-between">
              <!-- Copywrite Text -->
              <div class="copywrite-text">
                <p class="font-13">Created by <a href="https://www.instagram.com/gempong_/" target="_blank">Bagus Pramajaya.</a></p>
              </div>
              <div class="fotter-icon text-center">
                <p class="mb-0 font-13">Copyright 2020. All Rights Reserved.</p>
              </div>
            </footer>
          </div>
        </div>
      </div>
    </div>
    <!-- ======================================
      ********* Main Page Wrapper Area ***********
      ======================================= -->
    <!-- Plugins js -->
    <script src="/data/js/jquery.min.js"></script>
    <script src="/data/js/bootstrap.min.js"></script>
    <script src="/data/js/bundle.js"></script>
    <script src="/data/js/default-assets/setting.js"></script>
    <script src="/data/js/default-assets/date-time.js"></script>
    <!-- Active JS -->
    <script src="/data/js/default-assets/active.js"></script>
    <!-- Inject JS -->
    <script src="/data/js/default-assets/apexchart.min.js"></script>
    <script src="/data/js/script.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!--=== Active Js ===-->
    @yield('script')
    <script>
      $(document).on('click', 'a[href^="#"]', function(e) {
        var id = $(this).attr('href');

        var $id = $(id);
        if ($id.length === 0) {
            return;
        }
        e.preventDefault();

        var pos = $id.offset().top;

        $('body, html').animate({scrollTop: pos});
      });
     $.ajax({
       data : {
         '_token' : '{{ csrf_token() }}'
       },
       dataType: 'json',
        type: 'POST',
        url     : "{{route ('admin.visitors') }}",
        success: function(response){
        },
        error : function (response) {
        }
      });
    </script>
  </body>
</html>
