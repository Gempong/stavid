<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <title>{{ config('app.name', 'Laravel') }}</title>
   <link rel="stylesheet" type="text/css" href="/iofrm_files/bootstrap.min.css">
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
   <link rel="stylesheet" type="text/css" href="/iofrm_files/iofrm-style.css">
   <link rel="stylesheet" type="text/css" href="/iofrm_files/iofrm-theme16.css">
   </head>
   <body>
      <div class="form-body without-side">
         <div class="website-logo">
            <a href="/">
               <div class="logo">
                  <img class="logo-size" src="logo.svg" alt="">
               </div>
            </a>
         </div>
         <div class="row">
            <div class="img-holder">
               <div class="bg"></div>
               <div class="info-holder">
                  <img src="/iofrm_files/graphic3.svg" alt="">
               </div>
            </div>
            <div class="form-holder">
               <div class="form-content">
                  @yield('content')
               </div>
            </div>
         </div>
      </div>
      <script src="/iofrm_files/jquery.min.js"></script>
      <script src="/iofrm_files/popper.min.js"></script>
      <script src="/iofrm_files/bootstrap.min.js"></script>
      <script src="/iofrm_files/main.js"></script>
      <!-- <script>
        $(document).ready(function () {
          var width = $('.g-recaptcha').parent().width();
          if (width < 304) {
              var scale = width / 304;
              $('.g-recaptcha').css('transform', 'scale(' + scale + ')');
              $('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
              $('.g-recaptcha').css('transform-origin', '0 0');
              $('.g-recaptcha').css('-webkit-transform-origin', '0 0');
          }
        });
      </script> -->
   </body>
</html>
