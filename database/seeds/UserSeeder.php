<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new User([
            "name"      =>"Admin",
            "email"     =>"admin@admin.com",
            "password"  =>bcrypt('admin123'),
        ]);
        $user1->save();
    }
}
