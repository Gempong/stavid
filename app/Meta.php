<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meta extends Model
{
  public function uploadImage($image, $name) {
      $des    = 'upload/images/';
      $image->move($des, $name);
  }

  public function getImage() {
      $path   = 'upload/images/';
      return url($path.$this->logo);
  }
}
