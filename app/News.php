<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
  public function uploadImage($image, $name) {
      $des    = 'upload/images/';
      $image->move($des, $name);
  }

  public function getImage() {
      $path   = 'upload/images/';
      return url($path.$this->img);
  }

  public function date()
  {
      return date('d M Y H:i', strtotime($this->created_at));
  }

  public function content()
  {
      $des = stripslashes(strip_tags($this->content));
      return substr($des, 0, 100);
  }
}
