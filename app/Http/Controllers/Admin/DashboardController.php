<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Dashboard;
use App\Visitor;
use App\News;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $visitor_day = Visitor::whereDate('created_at', Carbon::today())->count();
        $visitor_month = Visitor::whereMonth('created_at', Carbon::today())->count();
        $visitor_all = Visitor::all()->count();

        $visitorData = Visitor::selectRaw("DATE_FORMAT(created_at, '%Y-%m-%d') as date, COUNT(*) as views")
              ->groupBy('date')
              ->orderBy('date', 'desc')
              ->get()
              ->take(7);

        $models = News::orderBy('created_at', 'desc')->get()->take(5);

        return view('admin.dashboard.index',compact('models','visitor_day','visitor_month','visitor_all','visitorData'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $data = Visitor::whereDate('created_at',date('Y-m-d'))
                       ->where('ip_address',$request->getClientIp())
                       ->count();

       if($data > 0){
         $response['msg'] = "Already";
       }else{
         $data = new Visitor();
         $data->ip_address = $request->getClientIp();
         $data->save();

         $response['msg'] = "Done";
       }
       return response()->json($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function show(Dashboard $dashboard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function edit(Dashboard $dashboard)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Dashboard $dashboard)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Dashboard  $dashboard
     * @return \Illuminate\Http\Response
     */
    public function destroy(Dashboard $dashboard)
    {
        //
    }
}
