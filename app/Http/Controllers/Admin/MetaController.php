<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Meta;
use Illuminate\Support\Str;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     protected function validationData($request){
         $this->validator      = Validator::make(
             $request,
             [
                 'title'          => 'required',
                 'keyword'            => 'required',
                 'description'            => 'required',
             ],
             [
                 'required'          => ':attribute is required.'
             ],
             [
                 'title'          => 'Site Title',
                 'keyword'            => 'Keyword',
                 'description'            => 'Description',
             ]
         );
     }

    public function index()
    {
      $models = Meta::first();
      return view('admin.meta.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Meta  $visitor
     * @return \Illuminate\Http\Response
     */
    public function show(Meta $visitor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Meta  $visitor
     * @return \Illuminate\Http\Response
     */
    public function edit(Meta $visitor)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Meta  $visitor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validationData($request->all());
      if ($this->validator->fails()) {
          return redirect()->back()->withInput($request->all())->withErrors($this->validator->errors());
      }

      $model = Meta::findOrFail($id);
      $model->title = $request->title;
      $model->keyword = $request->keyword;
      $model->description = $request->description;
      if($request->file('logo') != null){
        $name = $request->logo->getClientOriginalName();

        $model->uploadImage($request->file('logo'), $name);
        $model->logo = $name;
      }

      $model->save();

      return redirect()->route('admin.setting.index')->with('info', 'Success update data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Meta  $visitor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Meta $visitor)
    {
        //
    }
}
