<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\News;

class NewsController extends Controller
{

    public function post()
    {

        $data = array();
        $post = News::where('status', 1)->get();
        foreach($post as $item){
            $data_item = array();
            $data_item['id'] = $item->id;
            $data_item['img'] = $item->getImage();
            $data_item['title'] = $item->title;
            $data_item['slug'] = $item->slug;
            $data_item['created_at'] = $item->date();
            $data_item['content'] = $item->content();
            $data[] = $data_item;
        }

        return $data;
    }

}
