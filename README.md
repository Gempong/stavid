# STAVID

Website ini menampilkan data terbaru tentang perkembangan penyebaran virus corona di Indonesia secara detail, menampilkan total kasus, total sembuh, total meninggal dan kasus aktif di Indonesia.

## Installation

```bash
php artisan migrate
php artisan dump-autoload
php artisan db:seed
```

## Usage

```bash
php artisan serve
```
